# cme-tag
一个基于jquery的tag插件，具体的直接下载后运行gulp,用在浏览器里输入http://localhost:8000/src/index.html查看效果

# 依赖

* jquery

# 使用方法

* html里添加
```html

	<div id="tagGroup"></div>

```
* js(url在这个版本是必须的，暂时不支持已经构建好的html直接应用插件)
```javascript

$("#tagGroup").cmeTag({
	ajax : {
		url : ''
	}
	
});

```
* ajax 返回的格式

```javascript
[{
	key : 1,
	value : '专业',
	children : [{
		key : 2,
		value : '专业1'
	},{
		key : 3,
		value : '专业2'
	},{
		key : 4,
		value : '专业3'
	}]
}]
```

# 参数
参数 | 默认值 | 说明
----|------|----
el | | 指定tagId值存储的容器，支持jquery的方式，例如#tagIds
keyField | 'key'  | 获取从ajax返回的结果中取值，最终是通过getValue()获取所有选中的值
valueField | 'value'  | 获取从ajax返回的结果中取值，最终是显示在li中的文字
data |  | 用来初始化默认选中的tag，设置字符串，里面是每个tag的id，用逗号隔开，比如'1,2,3'
type | mult | 设置tag是单选还是多选，可选值mult,single
ajax.url | | tag数据来源url
ajax.type | | jquery的$.ajax的type
ajax.params | | 数据来源url参数，有两种形式：1.a=aaa&b=bbbb, 2.```  function(){ return {a:'aaa', b:'bbb'}} ```
beforeLoadData | function(data) | 从服务器获取数据后，在显示前可以修改数据，比如接口返回的数据格式转换成插件规定的格式
onTagClick | function(tagId,obj) | 每个tag点击后触发的事件，第一个参数是tagId，第二个参数是这个tag的jquery对象
style.label | | 为label添加样式class
style.ul | | 为生成的ul添加样式class
style.li | | 为生成的li添加样式class

# 方法
方法 | 参数 | 返回值
----|----|----
getValue | | 返回选中的tagId集合，格式化成字符串用逗号隔开：1,2,3

