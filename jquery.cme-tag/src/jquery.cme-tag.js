/**
* jquery cme-tag plugin
* author:huangyi
* version:1.0
* url : http://git.oschina.net/bigmouth/cme-tag
*/
;(function($){

	var defaultOptions = {
		keyField : 'key',
		valueField : 'value',
		data : '',
		type : 'mult',//mult,single
		el : '',
		ajax:{
			url:'',
			type : 'GET',
			params : ''
		},
		style : {
			label : '',
			ul : '',
			li : ''
		},
		beforeLoadData : function(data){
			return data;
		},
		onTagClick : function(tagId,obj){}
	}

	var tagManager = [];
	var groupManager = {};

	var setting = {};

	function CmeTag(opts){
		setting = $.extend({},defaultOptions,opts);
		loadData();
	}

	function initData(data){
		if(!!data){
			var dataAry = data.split(',');
			$.each(dataAry,(index,val)=>{
				if(setting.type == 'single'){
					//查询页面dom里tagId对应的groupId
					var groupId = $('li[tagId='+val+']',setting.$this).attr('groupId');
					groupManager[groupId] = val;
				}else{
					tagManager.push(val);
				}

			});
			if(setting.type == 'single'){
				for(var i in groupManager){
					if(groupManager.hasOwnProperty(i)){
						tagManager.push(groupManager[i]);
					}
				}
			}
			updateEl();
		}
	}
	/**arr util start**/
	function remove(arr,from, to) {
	  var rest = arr.slice((to || from) + 1 || arr.length);
	  arr.length = from < 0 ? arr.length + from : from;
	  return arr.push.apply(arr, rest);
	};
	function removeVal(arr,val){
		var index = arr.indexOf(val);
		if(index > -1){
			return remove(arr,index);
		}else{
			return arr;
		}
	}
	function contain(arr,val){
		return arr.indexOf(val) > -1;
	}
	/**arr util end**/

	function loadData(){
		var ajaxParam = '';
		if(!!setting.ajax.params){
			if(typeof setting.ajax.params == 'function'){
				ajaxParam = setting.ajax.params();
			}else{
				ajaxParam = setting.ajax.params;
			}
		}
		$.ajax({
			url : setting.ajax.url,
			dateType : 'json',
			type : setting.ajax.type,
			data : ajaxParam,
			success : function(data){
				var nData = setting.beforeLoadData(data);
				buildHtml(nData);
				initData(setting.data);
				if(!!tagManager){
					$.each(tagManager,(index,val)=>{
						highlight(val);
					});
				}
			}

		});
	}
	function buildHtml(data){
		setting.$this.addClass('cme-tag-wrapper');
		// <div class="cme-tag-group">
  //           <label class="cme-tag-label">tag分组名称</label>
  //           <ul class="cme-tag-ul">
  //               <li>标签1</li>
  //               <li>标签2</li>
  //               <li>标签3</li>
  //               <li>标签4</li>
  //               <li>标签5</li>
  //               <li class="active">标签6</li>
  //           </ul>
  //       </div>
  		//create group
  		$.each(data,(index,val)=>{
  			var groupId = val[setting.keyField];
  			//group div
  			var groupDiv = $('<div></div>');
  			groupDiv.addClass('cme-tag-group');
  			groupDiv.attr('id','#group'+groupId);
  			groupDiv.attr('groupId',groupId);
  			setting.$this.append(groupDiv);

  			//label
  			var label = $('<label></label>');
  			label.addClass('cme-tag-label');
  			if(!!setting.style.label){
  				label.addClass(setting.style.label);
  			}
  			label.html(val[setting.valueField]);
  			groupDiv.append(label);

  			//ul
  			var ul = $('<ul></ul>');
  			ul.addClass('cme-tag-ul');
  			if(!!setting.style.ul){
  				ul.addClass(setting.style.ul);
  			}
  			groupDiv.append(ul);

  			//li
  			if(!!val.children){
  				$.each(val.children,(index,valObj)=>{
  					var li = $('<li></li>');
  					li.attr('groupId',groupId);
  					li.attr('tagId',valObj[setting.keyField]);
  					if(!!setting.style.li){
		  				li.addClass(setting.style.li);
		  			}
  					//event
  					li.on('click',function(){
  						var $this = $(this);
  						var tagId = $this.attr('tagId');
  						var groupId = $this.attr('groupId');
  						if('mult' == setting.type){

	  						if(contain(tagManager,tagId)){
	  							removeVal(tagManager,tagId);
	  							removeHighLight(tagId);
	  						}else{
	  							tagManager.push(tagId);
	  							highlight(tagId);
	  						}
  						}else{
  							var oldTagId = groupManager[groupId];
  							//更新groupManager
  							removeHighLight(oldTagId);
  							groupManager[groupId] = tagId;
  							highlight(tagId);
  							//更新tagManager
  							removeVal(tagManager,oldTagId);
  							tagManager.push(tagId);
  						}
  						setting.onTagClick(tagId,$this);
  						updateEl();
  					});

  					li.html(valObj[setting.valueField]);
  					ul.append(li);
  				});
  			}
			  			
  		})
	}

	function highlight(key){
		$('li[tagId='+key+']',setting.$this).addClass('cme-tag-active');
	}
	function removeHighLight(key){
		$('li[tagId='+key+']',setting.$this).removeClass('cme-tag-active');	
	}

	function getValue(){
		return tagManager.join(',');
	}
	function updateEl(){
		if(!!setting.el){
			var el = $(setting.el);
			if(el.is('input')){
				el.val(getValue());
			}else{
				el.html(getValue());
			}
		}
	}

	$.fn.cmeTag = function(opts){
		opts.$this = $(this);
		var cmeTag = new CmeTag(opts);
		return {
			getValue : getValue
		}
	}

})(jQuery);