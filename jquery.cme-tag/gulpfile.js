var gulp = require('gulp'),
	connect = require('gulp-connect'),
	babel = require('gulp-babel'),
	clean = require('gulp-clean'),
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat'),
	minifyCSS = require('gulp-minify-css'),
	autoprefixer = require('gulp-autoprefixer');


gulp.task('reloadfile',function(){

	gulp.src('src/**/*.*')
		.pipe(connect.reload());
});

gulp.task('minjs',()=>{
	var stream = gulp.src('src/*.js')
		.pipe(babel({
			presets:['es2015']
		}))
		.pipe(concat('jquery.cme-tag.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('dist/js'));
	return stream;
});

gulp.task('js',()=>{
	var stream = gulp.src('src/*.js')
		.pipe(babel({
			presets:['es2015']
		}))
		.pipe(concat('jquery.cme-tag.js'))
		.pipe(gulp.dest('dist/js'));	
	return stream;
});

gulp.task('minCss',()=>{
	var stream = gulp.src('src/css/*.css')
		.pipe(minifyCSS())
		.pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
		.pipe(gulp.dest('dist/css'));
	return stream;
});
gulp.task('css',()=>{
	var stream = gulp.src('src/css/*.css')
		.pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
		.pipe(gulp.dest('dist/css'));

	return stream;
});

gulp.task('clean',()=>{
	var stream = gulp.src('dist')
		.pipe(clean());
	return stream;
});

gulp.task('connect',function(){
	connect.server({
		livereload: true,
		port: 8000
	});
});

gulp.task('watch',function(){

	gulp.watch('src/**/*.*',['dev']);
});

gulp.task('dev',['clean'],()=>{
	gulp.start('js','css','reloadfile');
});

gulp.task('default',['connect','clean'],()=>{
	gulp.start('js','css','watch');
});
gulp.task('release',['clean'],()=>{
	gulp.start('minjs','minCss');
});